import { useState } from "react";
import React from "react";

const Form = (props) => {
  const [value, setValue] = useState("");

  const handleChange = (evt) => {
    setValue(evt.target.value);
  };
  const handleSubmit = (evt) => {
    evt.preventDefault();
    onAdd(value);
  };
  const { onAdd } = props;
  return (
    <form onSubmit={handleSubmit}>
      <input className="new-todo" placeholder="Qu'avez vous à faire ?" onChange={handleChange} value={value} autoFocus />
      <input className="hidden" type="submit" value="Ajouter" />
    </form>
  );
};
export default Form;
