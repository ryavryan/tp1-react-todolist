import React from "react";
/*
li --> className="completed"
input --> className="toggle" type="checkbox" checked

li-> .. 
input --> className="toggle" type="checkbox" 

li -> className="editing"
input -->  className="toggle" type="checkbox"

*/
const TodoItem = (infos) => {
  return (
    <li className="completed">
      <div className="view">
        <input id={infos.id} className="toggle" type="checkbox" checked={infos.complete} />
        <label>{infos.name}</label>
        <button className="destroy" />
      </div>
      <form>
        <input className="edit" defaultValue={infos.name} />
        <input type="submit" value="Valider" className="hidden" />
      </form>
    </li>
  );
};

export default TodoItem;
