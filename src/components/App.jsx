import React from "react";
import TodoItem from "./TodoItem";
import Form from "./Form";
import { useState } from "react";
const App = () => {
  const todoList = [
    {
      id: "todo-1",
      key: "todo-1",
      name: "Tester React",
      completed: true,
    },
    {
      id: "todo-2",
      key: "todo-2",
      name: "Terminer le TP",
      completed: true,
    },
    {
      id: "todo-3",
      key: "todo-3",
      name: "Offrir du saucisson au prof",
      completed: false,
    },
  ];

  const [list, setList] = useState(todoList);
  const handleAdd = (todoName) => {
    const newTodoList = { ...todoList, id: "todo-" + todoList.length, key: "todo-" + todoList.length, name: todoName, completed: false };
    //alert("Nouveau todo : " + todoName);
    setList([...list, newTodoList]);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todo</h1>
        <Form onAdd={handleAdd}></Form>
      </header>
      {/* Cette section doit être cachée par défaut et affichée quand il y a des todos */}
      <section className="main">
        <input id="toggle-all" className="toggle-all" type="checkbox" />
        <label htmlFor="toggle-all">Tout compléter</label>
        <ul className="todo-list">
          {list.map((todo) => (
            <TodoItem key={todo.key} id={todo.id} name={todo.name} complete={todo.completed} />
          ))}
        </ul>
      </section>
      {/* Ce footer doit être caché par défaut et affichée quand il y a des todos */}
      <footer className="footer">
        {/* Ceci devrait être "0 restants" par défaut */}
        <span className="todo-count">
          <strong>2</strong> tâches restantes
        </span>
        <ul className="filters">
          <li>
            <button className="selected">Tous</button>
          </li>
          <li>
            <button>Actifs</button>
          </li>
          <li>
            <button>Complétés</button>
          </li>
        </ul>
        {/* Caché si aucun élément complété restant */}
        <button className="clear-completed">Effacer les complétés</button>
      </footer>
    </section>
  );
};

export default App;
